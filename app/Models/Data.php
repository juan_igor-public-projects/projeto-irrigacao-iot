<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $fillable = [
        'temperature',
        'humidity',
        'lumens',
        'soilHumidity',
        'weatherState',
        'weatherIntensity',
        'dataTime'
    ];

}
