<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IrrigationState extends Model
{
    protected $fillable = [
        'irrigate'
    ];
}
