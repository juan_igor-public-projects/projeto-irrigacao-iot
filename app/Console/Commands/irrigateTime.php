<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ProjectController;

class irrigateTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:irrigate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Testa se é hora de Irrigar e define o valor para o Banco de Dados';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new ProjectController();
        $controller->irrigate();
    }
}
