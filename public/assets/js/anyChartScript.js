function anyChartTemperature(){
        anychart.onDocumentReady(function () {
        // Create table to place gauges and information
        var layoutTable = anychart.standalones.table(1, 1);
        layoutTable.hAlign('center')
                .vAlign('middle')
                .useHtml(true)
                .fontSize(16)
                .cellBorder(null);

        // Set height and width for some cols and rows for layout table
        layoutTable.getRow(0)
                .height(350)
                .fontSize(16);
        
        // Put data and charts into the layout table
        layoutTable.contents([
                [drawLinearGauge(lastTemp)],
        ]);

        // Set container id and initiate drawing
        layoutTable.container('temperature-content');
        layoutTable.draw();

        // Helper function to create linear gauge
        function drawLinearGauge(value) {
                var gauge = anychart.gauges.linear();
                gauge.data([value]);
                gauge.tooltip(false);

                // Create thermometer title
                gauge.title()
                        .enabled(true)
                        .text(value + ' &deg;'+'C')
                        .fontColor('#212121')
                        .fontWeight(600)
                        .fontSize(18)
                        .orientation('bottom')
                        .useHtml(true)
                        .padding([10, 0, 5, 0]);

                // Create thermometer pointer
                var thermometer = gauge.thermometer(0);

                // Set thermometer settings
                thermometer.offset('1')
                        .width('3%')
                        .fill('#64b5f6')
                        .stroke('#64b5f6');

                // Set scale settings
                var scale = gauge.scale();
                scale.minimum(0)
                        .maximum(50)
                        .ticks({'interval': 10})
                        .minorTicks({'interval': 1});
                // Set axis and axis settings
                var axis = gauge.axis();
                axis.scale(scale)
                        .minorTicks(true)
                        .width('0')
                        .offset('0%');

                // Set text formatter for axis labels
                axis.labels()
                        .useHtml(true)
                        .format('{%Value}&deg;');

                return gauge
        }
        });

        // setTimeout(function(){
        //         var div = document.getElementsByClassName('anychart-credits');
        //         var i = 0;
        //         for(i=0; i<div.length; i++) div[0].parentNode.removeChild(div[0]);
        // }, 600);
}