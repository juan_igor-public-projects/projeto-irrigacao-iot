@extends('templates.html')

@section('head_links')

    <link rel="stylesheet" href="{{url('assets/css/style-index.css')}}" type="text/css"/>

    @if(intval(date('H')) > 5 || intval(date('H')) < 19)
        <link rel="stylesheet" href="{{url('assets/css/light-mode.css')}}" type="text/css"/>
    @else
        <link rel="stylesheet" href="{{url('assets/css/dark-mode.css')}}" type="text/css"/>
    @endif

    <script type="text/javascript" src="{{url('assets/js/functions.js')}}"></script>

    @include('includes.googleCharts')
    @include('includes.anychart')

    <script>
        let chartData_temp = {!! $chartTemp_data !!};
        let chartData_hum = {!! $chartHum_data !!};
        @if(count($chartTemp_data) > 1)
            let lastTemp = {!! $chartTemp_data[count($chartTemp_data)-1][1] !!};
        @else
            let lastTemp = 0;
        @endif
    </script>

@endsection

@section('content')

    <div id="title-div" class="flex-container flex-row">
        <img id="img-title" src="{{ url('assets/imgs/semb1.png') }}" />
        <h1 id="title">Irrigação IoT</h1>
    </div>
    <div id="system-info" class="grid-container">
        <div id="temperature-div" class="grid-item2 content-div">
            <h3>Temperatura: <i id="temp-img" class="fas fa-thermometer-half"></i></h3>
            <div id="temperature-content"></div>
        </div>
        <div id="info1_div" class="flex-container flex-row grid-item info-div">
            <div id="humidity-div" class="flex-item content-div">
                <h3>Umidade: <i class="fas fa-tint"></i></h3>
                <p id="humidity" class="show-info">0%</p>
            </div>
            <div id="lumen-div" class="flex-item content-div">
                <h3>Luminosidade: <i class="fas fa-sun"></i></h3>
                <p id="lumen" class="show-info">0 lúmens</p>
            </div>
            <div id="soil-humidity-div" class="flex-item content-div">
                <h3>Umidade do Solo: <i class="fas fa-water"></i></h3>
                <div class="row2-div">
                    <p id="soil-humidity" class="show-info">0%</p>
                </div>
            </div>
        </div>
        <div id="info2_div" class="flex-container flex-row grid-item info-div">
            <div id="rain-status-div" class="flex-item content-div">
                <h3>Estado do tempo: <i class="fas fa-cloud"></i></h3>
                <p><i>Estado:</i> <span id="rStatus">Não há dados!</span></p>
                <p id="pIntensity"><i>Intensidade:</i> <span id="rIntensity">Não há dados!</span></p>
            </div>
            <div id="irrigation-div" class="flex-item content-div">
                <h3>Irrigação do Solo: <i class="fas fa-seedling"></i></h3>
                <p id="last-irrigation" class="show-info"><i>Última irrigação:</i> <span id="last-irr">Não há dados!</span></p>
                <p id="next-irrigation" class="show-info"><i>Próxima irrigação:</i> <span id="next-irr">Não há dados!</span></p>
            </div>
        </div>
    </div>
    <div id="buttons" class="flex-container flex-row">
            <button id="irrigation-button" type="button" class="btn btn-dark" onclick="enviaIRRIGAR();"><i class="fas fa-seedling"></i> Irrigar Agora</button>
            <!-- <button id="status-button" type="button" class="btn btn-dark" onclick="enviaSTATUS();"><i class="fas fa-dice"></i> Gerar Valores</button> -->
            <button id="historic-button" type="button" class="btn btn-dark" data-toggle="modal" data-target="#historic-modal"><i class="fas fa-history"></i> Visualizar Histórico</button>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="historic-modal" tabindex="-1" role="dialog" aria-labelledby="HistoricoEmGraficos" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content" id="contentModal">
            <div class="modal-header">
            <div class="modal-title flex-container flex-row" id="HistoricoEmGraficos" style="align-content: center; align-items: center">
                <h5>Histórico em Gráficos</h5>
                <h5 class="resetZoom" onclick="clickMouse();">Clique aqui para resetar o Zoom</h5>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div id="bodyOfModal" class="modal-body">
                <div style="width: 100%">
                    <h5 style="text-align: center;">Gráfico de Temperatura</h5>
                    <div id="chart_div_temp" style="width: 100%; height: 100%; text-align: center;"></div>
                </div>
                <div style="width: 100%">
                    <h5 style="text-align: center; border-top: 1px solid lightgray; padding-top: 20px;">Gráfico de Umidade</h5>
                    <div id="chart_div_hum" style="width: 100%; height: 100%; text-align: center;"></div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        </div>
    </div>

@endsection

@section('after_content_scripts')

    <script type="text/javascript" src="{{url('assets/js/script.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/anyChartScript.js')}}"></script>

@endsection