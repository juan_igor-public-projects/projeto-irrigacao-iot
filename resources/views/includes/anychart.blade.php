<script src="{{ url('assets/anyChart/anychart-base.js') }}"></script>
<script src="{{ url('assets/anyChart/anychart-ui.js') }}"></script>
<script src="{{ url('assets/anyChart/anychart-exports.js') }}"></script>
<script src="{{ url('assets/anyChart/anychart-linear-gauge.js') }}"></script>
<script src="{{ url('assets/anyChart/anychart-circular-gauge.js') }}"></script>
<script src="{{ url('assets/anyChart/anychart-table.js') }}"></script>
<link href="{{ url('assets/anyChart/anychart-ui.css') }}" type="text/css" rel="stylesheet">
<link href="{{ url('assets/anyChart/anychart-font.css') }}" type="text/css" rel="stylesheet">
