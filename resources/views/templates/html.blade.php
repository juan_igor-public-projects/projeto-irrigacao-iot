<!DOCTYPE html>

<html lang="pt-br">
    <head>
        {{-- <link rel="icon" href="{{ url('assets/imgs/favicon.jpg') }}"> --}}
        <link rel="icon" href="{{ url('assets/imgs/semb11-small.png') }}">
<!--        <link rel="icon" href="{{ url('assets/imgs/favicon.jpg') }}" />-->
        <!--<link rel="icon" type="image/vnd.microsoft.icon" href="{{ url('assets/imgs/favicon.jpg') }}" />-->
        <!--<link rel="SHORTCUT ICON" href="{{ url('assets/imgs/favicon.jpg') }}" />-->
        <title>{{$title ?? 'IoT Irrigation'}}</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="{{url('assets/material-design-icons/css/material-icons.css')}}"/>
        <link rel="stylesheet" href="{{url('assets/bootstrap/dist/css/bootstrap.css')}}"/>
        <script src="{{ url('assets/jQuery/jquery-3.4.0.js') }}"></script>
        @include('includes.fontAwesome')
        @yield('head_links')
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
        @yield('after_content_scripts')
    </body>
</html>



